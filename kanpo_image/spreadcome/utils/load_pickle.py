import pickle

def load_pickle(path):
    with open(path, "rb") as file:
        arr = pickle.load(file)
    return arr

import os
import pandas as pd 

class SCDataLoader:
    
    def __init__(self, excel_path):
        self.excel_path = excel_path 
        
        if not(os.path.exists(self.excel_path)):
            print("This excel path is not exist")
        else:
            print("Read excel successfully")
            self.refresh()
    
    def refresh(self):
        self.data = pd.read_excel(self.excel_path, engine="openpyxl")
    
    def create_data(self, train_path, cols_name):
        self.user_train = []
        self.data = self.data[cols_name]

        # Remove null
        self.data = self.data.drop(self.data[(self.data.DrugName1.isnull()) & (self.data.DrugName2.isnull())].index)

        X = []
        y = []
        
        if train_path is None:
            for line in self.data.to_numpy():
                X.append(line[:-2])
                y.append(line[-2::1]) 
        else:
            with open(train_path, "r") as f:
                self.user_train = [user_id[:-1] for user_id in f.readlines()]
                
            for line in self.data.to_numpy():
                user_id = line[0]
                
                if user_id in self.user_train:
                    X.append(line[:-2])
                    y.append(line[-2::1])
                
        return (X, y)

    def one_leave_kfold(self, index, cols_name):
        self.data = self.data[cols_name]

        # Remove null
        self.data = self.data.drop(self.data[(self.data.DrugName1.isnull()) & (self.data.DrugName2.isnull())].index)

        X = []
        y = []
        X_test = []
        y_test = []

        count = 0
            
        for line in self.data.to_numpy():
            if count == index:
                X_test.append(line[:-2])
                y_test.append(line[-2::1])
            else:
                X.append(line[:-2])
                y.append(line[-2::1])

            count += 1
                
        return X, y, X_test, y_test
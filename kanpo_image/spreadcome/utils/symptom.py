import pandas as pd
from spreadcome.utils.util import is_ignore 

def get_all_symptom():
    ans = []
    taisitu_df = pd.read_csv("spreadcome/raw_data/taisitu_data.csv")

    for mental_test in taisitu_df["Test"]:
        if is_ignore(mental_test):
            continue 

        ans = ans + mental_test.split("\u3000")

    ans = list(set([j for j in ans if not(is_ignore(j))]))

    return ans 

def get_all_worries():
    user_df = pd.read_csv("spreadcome/raw_data/user_data.csv")
    ignore_list = ["nan", " ", "", "//"]

    symptom_list = []

    for symptom_user in user_df["Symptom"]:
        if str(symptom_user) in ignore_list:
            continue
        
        symptom_list = symptom_list + symptom_user.split("\u3000")

    symptom_list = list(set(symptom_list))

    return [j for j in symptom_list if str(j) not in ignore_list and not(j.isdigit())]


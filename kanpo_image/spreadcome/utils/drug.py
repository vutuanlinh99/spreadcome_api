import pandas as pd 
from spreadcome.utils.util import is_ignore

def get_full_drug_name(get_element=True):
    drug_csv = pd.read_csv("spreadcome/raw_data/full_drug.csv")
    drug_list = list(drug_csv["Drug"])

    if get_element:
        split_char = "＋"
        element_list = []

        for drug in drug_list:
            element_list = element_list + drug.split(split_char)

        element_list = list(set(element_list + drug_list))

        return element_list
    else:
        return drug_list




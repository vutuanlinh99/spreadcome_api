import pandas as pd 
from spreadcome.utils.drug import get_full_drug_name
from spreadcome.utils.symptom import get_all_worries
from spreadcome.utils.util import create_timestamp
from spreadcome.utils.util import is_ignore
import numpy as np

# WARNING: this file is only used for preprocessing the first raw data provided by the customer.
# Therefore, please do not use it or Pipeline class in this file. 
# It may cause some error in the code.

class PipeLine:

    def __init__(self):
        self.element_list = get_full_drug_name()
        self.worries_list = get_all_worries()
        self.user_df = pd.read_csv("spreadcome/raw_data/user_data.csv")

        self.message_df = pd.read_excel("spreadcome/raw_data/message.xlsx", engine="openpyxl")
        self.message_df = self.message_df[self.message_df.type == "回答"]

        self.order_df = pd.read_csv("spreadcome/raw_data/order_data.csv")
        self.taisitu_df = pd.read_csv("spreadcome/raw_data/taisitu_data.csv")

    def extract_message_with_drug(self, drug_list, message_df):
        user_list = []
        message_list = []
        drug_in_mess = []
        times = []

        for line in message_df.to_numpy():
            mess = line[2]
            user_id = line[0]
            time = line[3]
            ans_drug = []

            for drug in drug_list:
                if drug in mess:
                    ans_drug.append(drug)
            
            if len(ans_drug) > 0:
                user_list.append(user_id)
                message_list.append(mess)
                drug_in_mess.append(", ".join(ans_drug))
                times.append(time)

        # Create pd.DataFrame 
        table = pd.DataFrame()
        table["Id"] = pd.Series(user_list)
        table["Message"] = pd.Series(message_list)
        table["Drug"] = pd.Series(drug_in_mess)
        table["Timestamp"] = pd.Series(times)

        print(len(table))
        return table 

    def merge_mess_to_order(self, order_df, message_df):
        ans_df = order_df.copy() 

        message_list = []
        miss = 0

        for line in ans_df.to_numpy():
            user_id = line[0]
            timestamp = line[-1]
            order_time = create_timestamp(timestamp)

            df = message_df[message_df.Id == user_id]
            df_array = df.to_numpy()[-1::-1]
            is_match = False

            for i in df_array:
                timestamp = i[-1]
                type = i[1]
                df_time = create_timestamp("{}/{}/{} {}:{}:{}".format(timestamp.year, timestamp.month, timestamp.day, timestamp.hour, timestamp.minute, timestamp.second))

                if df_time <= order_time:
                    if type != "相談":
                        message_list.append(i[-3])
                        is_match = True 
                        break 

            if not(is_match):
                message_list.append(np.nan)
                miss += 1 
        
        print("Miss rate: {}".format(miss*100/len(ans_df)))
        ans_df["Message"] = pd.Series(message_list)

        return ans_df

    def filter_null_value(self, df):
        ans = df.copy()
        name_cols = df.columns

        if "Id" in name_cols:
            ans = ans.drop(ans[ans.Id.isnull()].index)
        
        if "Message" in name_cols:
            ans = ans.drop(ans[ans.Message.isnull()].index)

        if "DrugName1" in name_cols:
            ans = ans.drop(ans[ans.DrugName1.isnull()].index)

        return pd.DataFrame(ans.to_numpy(), columns=name_cols)

    def extract_message_with_worries(self, message_df):
        worry_in_mess = []
        ans = message_df.copy()

        for line in message_df.to_numpy():
            mess = line[-1]
            ans_worries = []

            for worry in self.worries_list:
                if worry in mess:
                    ans_worries.append(worry)

            if len(ans_worries) > 0:
                worry_in_mess.append(", ".join(ans_worries))
            else:
                worry_in_mess.append(np.nan)

        ans["Worries"] = pd.Series(worry_in_mess)
        ans = ans.drop(["Using", "Message"], axis=1)
        return ans

    def merge_taisitu_to_message(self, taisitu_df, message_df):
        taisitu = taisitu_df.copy()
        ans = message_df.copy()

        symptom_list = []
        mental_values = []

        for line in ans.to_numpy():
            user_id = line[0]
            timestamp = line[-2]
            order_date = create_timestamp(timestamp)

            df = taisitu[taisitu.Id == user_id]
            df_array = df.to_numpy()[-1::-1]
            is_match = False 

            for i in df_array:
                timestamp = i[-1]
                taisitu_date = create_timestamp(timestamp)

                if order_date >= taisitu_date:
                    is_match = True 

                    if is_ignore(str(i[1])):
                        symptom_list.append(np.nan) 
                    else:
                        symptom_list.append(i[1])

                    mental_values.append(i[-9:-1:1])
                    break

            if not(is_match):
                symptom_list.append(np.nan)
                mental_values.append(np.array([np.nan]*8))

        ans["Mental_Test"] = pd.Series(symptom_list)
        ans[['気虚', '陽虚', '血虚', '陰虚', '気滞', '湿熱', '血瘀', '湿痰']] = pd.DataFrame(np.array(mental_values))

        return ans

    def add_gender(self, df):
        ans = df.copy()
        gender_list = []

        for i in range(len(ans)):
            info = ans.iloc[i]
            user_id = info["Id"]

            gender = self.user_df[self.user_df.Id == user_id]["Gender"].to_numpy()[0]
            gender_list.append(gender)

        ans["Gender"] = pd.Series(gender_list)

        return ans

    def show_missing(self, df):
        print(df.isnull().sum()/len(df))
    
    def preprocessing(self):
        ans = self.extract_message_with_drug(self.element_list, self.message_df)
        ans = self.merge_mess_to_order(self.order_df, ans)
        ans = self.filter_null_value(ans)
        ans = self.extract_message_with_worries(ans)
        ans = self.merge_taisitu_to_message(self.taisitu_df, ans)
        ans = ans.drop(ans[ans.Mental_Test.isnull()].index)
        ans = pd.DataFrame(ans.to_numpy(), columns=ans.columns)
        ans = self.add_gender(ans)
        self.show_missing(ans)
        return ans
    

    
    

    


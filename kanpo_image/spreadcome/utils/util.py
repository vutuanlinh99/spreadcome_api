import datetime

def is_ignore(string):
    ignore_list = ["nan", " ", "", "//"]

    if str(string) in ignore_list:
        return True 
    
    return False

def create_timestamp(string):
    info = string.split(" ")
    date, time = info[0], info[-1]

    year, month, day = date.split("/")
    hour, minute, second = time.split(":")

    return datetime.datetime(int(year), int(month), int(day), \
        int(hour), int(minute), int(second))


def add_zero(string):
    return "0" + string if len(string) < 2 else string

def convert_datetime_to_str(timestamp):
    year = str(timestamp.year) 
    month = add_zero(str(timestamp.month))
    day = add_zero(str(timestamp.day))
    hour = add_zero(str(timestamp.hour))
    minute = add_zero(str(timestamp.minute))
    second = add_zero(str(timestamp.second))

    return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second

from spreadcome.utils.symptom import get_all_symptom, get_all_worries
from spreadcome.utils.util import is_ignore
import datetime
import numpy as np
from math import pi

class Converter: 

    def __init__(self, symptom_list=get_all_symptom(), worries_list=get_all_worries(), gender_list=["男性", "女性", "他の"]):
        # These attributes are used to convert string of gender, symptom or worries to one hot vector 
        self.symptom_list = symptom_list
        self.worries_list = worries_list
        self.gender_list = gender_list 

        # Below attributes are used for normalizing timestamp and 8 mental values 
        self.max_year = datetime.date.today().year 
        self.list_max = np.array([50, 50, 53, 62, 60, 62, 49, 55])

    def convert_str_to_vec(self, feature_list, strings, delimiter, name):
        ans = np.zeros((len(strings), len(feature_list)))

        for idx, string in enumerate(strings):
            if is_ignore(string):
                continue

            for feature in string.split(delimiter):
                if not(is_ignore(feature)) and feature in feature_list:
                    ans[idx, feature_list.index(feature)] = 1

        return ans

    def convert_int(self, x):
        return [int(i) for i in x]

    def normalize_mental_values(self, data):
        data = np.apply_along_axis(self.convert_int, 1, data)
        return data/self.list_max

    def preprocessing(self, data):
        gender_part = data[:, 1]
        worries_part = data[:, 2]
        mental_test_part = data[:, 3]
        mental_values_part = data[:, 4:12]
        timestamp_part = data[:, -1]

        gender_part = self.convert_str_to_vec(self.gender_list, gender_part, " ", "gender")
        # print("gender",gender_part )
        worries_part = self.convert_str_to_vec(self.worries_list, worries_part, ", ", "worries")
        # print("worries", worries_part)
        mental_test_part = self.convert_str_to_vec(self.symptom_list, mental_test_part, "\u3000", "mental_test")
        mental_values_part = self.normalize_mental_values(mental_values_part)
        timestamp_part = self.to_datetime_vec(timestamp_part.reshape(-1, 1))
        
        return np.concatenate([gender_part, worries_part, mental_test_part, mental_values_part, timestamp_part], axis=1)

    def _to_datetime_vec(self, timestamp):
        info = timestamp[0].split(" ")
        date = info[0]
        max_month = 12
        
        year, month, day = map(int, date.split("/"))
        day = self.sin_scaler(day, period=31)
        month = month/max_month
        year = year/self.max_year

        return np.array([year, month, day])

    def sin_scaler(self, data, period):
        x = data*pi/period

        return np.abs(np.sin(x))

    def to_datetime_vec(self, data):
        data = np.apply_along_axis(self._to_datetime_vec, 1, data)

        return data


    

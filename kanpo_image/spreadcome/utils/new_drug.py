import pandas as pd
import json
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from collections import Counter

drug = pd.read_csv("spreadcome/raw_data/drug_ingredient.csv", on_bad_lines='skip')

drug['ingredient'] = drug['ingredient'].str.replace("\u3000"," ")

history_drug = pd.read_csv("spreadcome/raw_data/full_drug.csv", on_bad_lines='skip')


list_mental = ['気滞', '湿熱', '血お', '湿痰', '気虚', '陽虚', '血虚', '陰虚']
list_all_drug = list(drug['drug_name'].unique())
list_history_drug = list(history_drug['Drug'].unique())


with open("spreadcome/raw_data/mental_reject_dict.json", "r") as fp:
    mental_reject_dict = json.load(fp)

with open("spreadcome/raw_data/all_combine_drug.json", "r") as fp:
    all_combine_drug = json.load(fp)

def calculate_score (text_1, text_2):
    try:
        text = [text_1, text_2]
        cv = CountVectorizer(ngram_range=(1, 2))
        count_matrix = cv.fit_transform(text)
        score = round(cosine_similarity(count_matrix)[0][1],4)
    except ValueError :
        score = 0
    return score

def get_new_drug (ans, list_value):
    list_reject = []
    mental_value = dict(zip(list_mental, list_value))
    for k in list(mental_value.keys()):
        if mental_value[k] > 13:
            list_reject += mental_reject_dict[k]

    list_drug_accept = list(set(list_all_drug) - set(list_reject))
    df_drug_accept = drug[drug['drug_name'].isin(list_drug_accept)]
    combine_drug = get_combine_drug(df_drug_accept)
    list_drug_effect = [get_drug_effect(an, all_combine_drug) for an in ans]
    drug_effect_selected = list_drug_effect[0]
    list_recommend =  get_drug(drug_effect_selected, combine_drug, ans)
    return list_recommend

def get_combine_drug(df_drug_accept):
    combine_drug = {}
    drug_arr = df_drug_accept.to_numpy()
    for  i in range(drug_arr.shape[0]):
         j = i + 1
         while j < drug_arr.shape[0]:
            if str(drug_arr[i][0]) == str(drug_arr[j][0]):
                combine_drug[str(drug_arr[i][0])] = str(drug_arr[i][1])
            else:
                combine_drug[str(drug_arr[i][0])+"＋"+str(drug_arr[j][0])] = str(drug_arr[i][1])+"＋"+str(drug_arr[j][1])
            j += 1
    return combine_drug


def get_drug(effect_drug, combine_drug, ans):
    res = {}
    for i in list(combine_drug.keys()):
        reverse_i = '＋'.join(i.split('＋')[::-1])
        if i not in list_history_drug and i not in ans and reverse_i not in list_history_drug and reverse_i not in ans:
            score = calculate_score(effect_drug, combine_drug[i])
            res[i] = score

    counter = Counter(res)
    top25 = [i[0] for i in counter.most_common(25)]
    return top25[0:3]


def get_drug_effect(name_drug, combine_drug):
    reverse_name = '＋'.join(name_drug.split('＋')[::-1])
    print(name_drug)
    print(reverse_name)
    try:
        effect = combine_drug[name_drug]
    except KeyError:
        effect = combine_drug[reverse_name]
    return effect
import numpy as np

class Distance:
    # [1, 1, 1.5, 1, 1]: 88.12%
    # [1.5, 1.5, 1.5, 1.5, 0.5]: 88.37%
    def __init__(self, weights=[1.5, 1.5, 1.5, 0.5, 0.5], is_weight=True):
        self.weights = weights
        self.is_weight = is_weight

    def get_used_weight(self):
        return self.is_weight

    def calculate_euclid(self, user, others, name="euclid"):
        ans = np.sqrt(np.sum((user - others)**2, axis=1).astype(np.float32))
        return ans

    def calculate_hamming(self, user, others, name="hamming"):
        user = user.astype(np.int64)
        others = others.astype(np.int64)

        diff = np.logical_xor(user, others)
        diff = np.where(diff==True, 1, 0)
        dis = np.sum(diff, axis=1) 

        return dis 
    
    # This code need to refactor because it fixed (Should not change this if not understand)
    def calculate_distance(self, user, others):
        others = others[np.where(np.sum(others[:,3:206],axis=1) > 0)[0]]
        gender_part = others[:, :3]
        worries_part = others[:, 3:206]
        mental_test_part = others[:, 206:281]
        mental_values_part = others[:, -11:-3:1]
        timestamp_part = others[:, -3::1]
        print("gender score",  np.sum(self.calculate_hamming(user[:3], gender_part)))
        print("worries score",np.sum(self.calculate_hamming(user[3:206], worries_part)))
        print("mental_test_part", np.sum(self.calculate_hamming(user[206:281], mental_test_part), axis=0))
        print(" mental_values_part", np.sum(self.calculate_hamming(user[-11:-3:1], mental_values_part), axis=0))
        print("timestamp_part", np.sum(self.calculate_hamming(user[:3], timestamp_part)))
        # return self.calculate_euclid(user[:3], gender_part)+ self.calculate_euclid(user[3:206], worries_part) + \
        #     self.calculate_euclid(user[206:281], mental_test_part) + self.calculate_euclid(user[-11:-3:1], mental_values_part) + \
        #         self.calculate_euclid(user[-3::1], timestamp_part)




        return self.calculate_hamming(user[:3], gender_part)*self.weights[0] + self.calculate_hamming(user[3:206], worries_part)*self.weights[1] + \
            self.calculate_hamming(user[206:281], mental_test_part)*self.weights[2] + self.calculate_euclid(user[-11:-3:1], mental_values_part)*self.weights[3] + \
                self.calculate_euclid(user[-3::1], timestamp_part)*self.weights[4]

        # return self.calculate_euclid(user[:3], gender_part)*self.weights[0] + self.calculate_euclid(user[3:206], worries_part)*self.weights[1] + \
        #     self.calculate_euclid(user[206:281], mental_test_part)*self.weights[2] + self.calculate_euclid(user[-11:-3:1], mental_values_part)*self.weights[3] + \
        #         self.calculate_euclid(user[-3::1], timestamp_part)*self.weights[4]


    def calculate_weight(self, dis):
        eps = 0.001
        return 1/(eps + dis)
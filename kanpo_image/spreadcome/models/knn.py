import numpy as np 
from spreadcome.utils.util import is_ignore
# from spreadcome.utils.handler_raw import PipeLine

class KanpoKNN:

    def __init__(self, X, y, distance=None, n_neighbor=10, n_ans=3):
        # Check if we init distance object or not 
        assert distance is not None, "Must init distance"

        # X: [Id, Gender, Worries, Mental_Test, '気虚', '陽虚', '血虚', '陰虚', '気滞', '湿熱', '血瘀', '湿痰', Timestamp]
        # X: User info
        self.X = X
        # y: [DrugName1, DrugName2, ...]
        # y: Drug which was bought or a prescription in the past
        self.y = y 

        # Object distance for calculating the distance between each user.
        self.distance = distance 

        # For the number of other users used for recommending drugs for patience. 
        self.n_neighbor = n_neighbor

        # For the number of precriptions which we recommend
        self.n_ans = n_ans 

    # This function is used to recommend drug for patience    
    def predict(self, user):
        # If this KNN uses distance weight or not
        use_weight = self.distance.get_used_weight()

        # Calculating the distance between user and other patiences in the database
        dis_list = self.distance.calculate_distance(user, self.X)
        # print("user", user)

        # Sorting the dis_list from small to large if use distance weight else large to small
        dis_list = np.concatenate([dis_list.reshape(-1, 1), np.array(range(len(dis_list))).reshape(-1, 1)], axis=1)

        if use_weight:
            dis_list = sorted(dis_list, key=lambda x: x[0])[:self.n_neighbor]
        else:
            dis_list = sorted(dis_list, key=lambda x: x[0], reverse=True)[self.n_neighbor]
        print("dis list ", dis_list)
        # Voting for each drug of the neighbors
        recommend_ans = []
        score_dict = dict()
        # print("KKKKKKK", score_dict)
        for item in dis_list:
            index = item[1]
            dis = item[0]
            # print(("index %f, item %f")%(index, dis))
            for drug in self.y[int(index)]:
                # print("drug ", drug)
                if not(is_ignore(drug)):
                    if drug not in score_dict:
                        score_dict[drug] = self.distance.calculate_weight(dis) if use_weight else 1 
                    else:
                        score_dict[drug] += self.distance.calculate_weight(dis) if use_weight else 1

        # print("Score dict ", score_dict)
        print("len score", len(score_dict))
        # Get top 3 drugs, recommend format: [drug_name, score]
        t = sorted(score_dict.items(), key=lambda x: x[1], reverse=True)[:10]
        print("score dict",t)
        recommend_ans = sorted(score_dict.items(), key=lambda x: x[1], reverse=True)[:self.n_ans]
        # print("recommend ans", recommend_ans)
        # pl = PipeLine()
        # pl.preprocessing()
        return [drug_info[0] for drug_info in recommend_ans]

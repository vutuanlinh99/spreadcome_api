from fastapi import FastAPI, Request
from spreadcome.models.knn import KanpoKNN
from spreadcome.utils.load_pickle import load_pickle
from spreadcome.utils.new_drug import get_new_drug
from spreadcome.utils.distance import Distance
from spreadcome.utils.util import convert_datetime_to_str
from spreadcome.utils.converter import Converter 
import time 
import numpy as np 
from datetime import datetime
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
x_path = "spreadcome/pickle_data/x.pickle"
y_path = "spreadcome/pickle_data/y.pickle"

X = load_pickle(x_path)
y = load_pickle(y_path)

converter = Converter()
model = KanpoKNN(X, y, Distance())

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.put("/update_pickle/")
async def update_pickle():
    pass 

@app.put("/update_model/")
async def update_model():
    global x_path, y_path, X, y, model
    X = load_pickle(x_path)
    y = load_pickle(y_path)
    model = KanpoKNN(X, y, Distance())

@app.get("/recommend_drug/")
async def recommend_drug(request: Request):
    # X: ["Id", "Gender", "Worries", "Mental_Test", '気虚', '陽虚', '血虚', '陰虚', '気滞', '湿熱', '血瘀', '湿痰', "Timestamp"]
    # return {"prescriptions": ans, "end_time": end, "inference_time": end - start}
    gender = request.query_params["gender"]
    worries = request.query_params["worries"]
    mental_test = request.query_params["mental_test"]
    qi_value = request.query_params["気虚"]
    pos_value = request.query_params['陽虚']
    blood_less_value = request.query_params['血虚']
    negative_value = request.query_params['陰虚']
    delay_value = request.query_params['気滞']
    temp_hum_value = request.query_params['湿熱']
    blood_value = request.query_params['血瘀']
    wet_phelm_value = request.query_params['湿痰']
    timestamp = datetime.now()
    timestamp = convert_datetime_to_str(timestamp) 
    id = -1 

    user_info = [[id, gender, worries, mental_test, int(qi_value), int(pos_value), \
        int(blood_less_value), int(negative_value), int(delay_value), int(temp_hum_value), \
            int(blood_value), int(wet_phelm_value), timestamp]]

    # print("user: {}".format(user_info))

    start = time.time()
    preprocessed_user = converter.preprocessing(np.array(user_info))
    ans = model.predict(np.array(preprocessed_user[0]))
    # print("Old: ",ans)
    # new_drug = get_new_drug(ans, [int(qi_value), int(pos_value),  int(blood_less_value), int(negative_value),int(delay_value), int(temp_hum_value),int(blood_value), int(wet_phelm_value)])
    # print("new: ", new_drug)
    end = time.time()

    # return {"old_prescriptions": ans, "new_prescriptions": new_drug, "end_time": end, "inference_time": end - start}
    return {"old_prescriptions": ans,  "end_time": end, "inference_time": end - start}

